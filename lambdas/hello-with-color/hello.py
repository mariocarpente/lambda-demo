import color


def lambda_handler(event, context):
   message = "Hello world! "+color.get_text_color()
   return {
       'message' : message
   }