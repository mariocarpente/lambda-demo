provider "aws" {}

resource "aws_iam_role" "lambda_role" {
name   = "Demo_Lambda_Function_Role"
assume_role_policy = <<EOF
{
 "Version": "2012-10-17",
 "Statement": [
   {
     "Action": "sts:AssumeRole",
     "Principal": {
       "Service": "lambda.amazonaws.com"
     },
     "Effect": "Allow",
     "Sid": ""
   }
 ]
}
EOF
}

resource "aws_iam_policy" "iam_policy_for_lambda" {
 
 name         = "aws_iam_policy_for_terraform_aws_demo_lambda_role"
 path         = "/"
 description  = "AWS IAM Policy for managing aws lambda role"
 policy = <<EOF
{
 "Version": "2012-10-17",
 "Statement": [
   {
     "Action": [
       "logs:CreateLogGroup",
       "logs:CreateLogStream",
       "logs:PutLogEvents"
     ],
     "Resource": "arn:aws:logs:*:*:*",
     "Effect": "Allow"
   }
 ]
}
EOF
}
 
resource "aws_iam_role_policy_attachment" "attach_iam_policy_to_iam_role" {
 role        = aws_iam_role.lambda_role.name
 policy_arn  = aws_iam_policy.iam_policy_for_lambda.arn
}

data "archive_file" "zip_hello_code" {
type        = "zip"
source_file  = "${path.module}/lambdas/hello.py"
output_path = "${path.module}/lambdas/hello.zip"
}
 
resource "aws_lambda_function" "demo_hello_lambda_func" {
filename                       = "${path.module}/lambdas/hello.zip"
function_name                  = "demo-hello-python"
role                           = aws_iam_role.lambda_role.arn
handler                        = "hello.lambda_handler"
runtime                        = "python3.9"
publish                        = true
depends_on                     = [aws_iam_role_policy_attachment.attach_iam_policy_to_iam_role]
}

resource "aws_lambda_alias" "demo_hello_lambda_alias" {
  name             = "latest"
  description      = "Latest version deployed"
  function_name    = aws_lambda_function.demo_hello_lambda_func.arn
  function_version = aws_lambda_function.demo_hello_lambda_func.version
}


data "archive_file" "zip_hello_with_color_code" {
type        = "zip"
source_dir  = "${path.module}/lambdas/hello-with-color/"
output_path = "${path.module}/lambdas/hello-with-color.zip"
}
 
resource "aws_lambda_function" "demo_hello_with_color_lambda_func" {
filename                       = "${path.module}/lambdas/hello-with-color.zip"
function_name                  = "demo-hello-with-color-python"
role                           = aws_iam_role.lambda_role.arn
handler                        = "hello.lambda_handler"
runtime                        = "python3.9"
publish                        = true
depends_on                     = [aws_iam_role_policy_attachment.attach_iam_policy_to_iam_role]
}

resource "aws_lambda_alias" "demo_hello_with_color_lambda_alias" {
  name             = "latest"
  description      = "Latest version deployed"
  function_name    = aws_lambda_function.demo_hello_with_color_lambda_func.arn
  function_version = aws_lambda_function.demo_hello_with_color_lambda_func.version
}
